# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: adubois <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/19 05:45:23 by adubois           #+#    #+#              #
#*   Updated: 2016/01/22 13:48:48 by adubois          ###   ########.fr       *#
#                                                                              #
# **************************************************************************** #

NAME = fractol
CC = gcc
FLAGS = -Wall -Werror -Wextra
INC_PATH = -I./inc/ -I./libmlx/ -I./libft/
LIB_PATH = -L./libmlx/ -lmlx \
		   -L./libft/ -lft \
		   -framework OpenGL -framework AppKit
SRC_PATH = ./src
OBJ_PATH = ./obj
SRC = 	fractol.c \
		hooks.c \
		fractals.c \
		draw.c \
		utils.c \
		rect.c

OBJ = $(SRC:%.c=$(OBJ_PATH)/%.o)

all: make

make: libs $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(FLAGS) $(INC_PATH) $(LIB_PATH)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) -o $@ -c $< $(FLAGS) $(INC_PATH)

clean:
	rm -rf $(OBJ_PATH)

fclean: clean
	rm -f $(NAME)

re: fclean all

libs:
	make -C libft
	make -C libmlx

libs-clean:
	make -C libft clean
	make -C libmlx clean
	
libs-fclean: libs-clean
	make -C libft fclean
	
libs-re: libs-fclean
	make -C libft re
	make -C libmlx re

.PHONY: all clean fclean re libs-clean libs-fclean libs-re
