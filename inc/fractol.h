/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 05:47:06 by adubois           #+#    #+#             */
/*   Updated: 2016/01/22 13:54:34 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <stdlib.h>
# include "mlx.h"
# include "libft.h"

# define WIN_WIDTH		800
# define WIN_HEIGHT		800
# define WIN_TITLE		"Fract'ol"
# define DEFAULT_DWELL	100

# define NO_EVENT_MASK			0L
# define KEY_PRESS_MASK			(1L<<0)
# define KEY_RELEASE_MASK		(1L<<1)
# define BUTTON_PRESS_MASK		(1L<<2)
# define BUTTON_RELEASE_MASK	(1L<<3)
# define POINTER_MOTION_MASK	(1L<<6)
# define EXPOSURE_MASK			(1L<<15)

# define KEY_PRESS			2
# define KEY_REALEASE		3
# define BUTTON_PRESS		4
# define BUTTON_RELEASE		5
# define MOTION_NOTIFY		6
# define EXPOSE				12

typedef struct	s_rect
{
	float		x_min;
	float		x_max;
	float		y_min;
	float		y_max;
}				t_rect;

typedef struct	s_window
{
	struct s_window	*next;
	void			*ptr;
	void			*mlx;
	int				(*fractals[3])(struct s_window *win);
	t_rect			rect;
	void			*img;
	char			*data;
	int				bpp;
	int				lsize;
	int				bendian;
	int				dwell;
	unsigned int	frac_type;
	int				iter;
	int				mouse_x;
	int				mouse_y;
	float			var_x;
	float			var_y;
	float			real;
	float			imag;
}				t_window;

typedef struct	s_fractol
{
	void		*mlx;
	t_window	*win;
}				t_fractol;

void			*new_window(void *mlx, t_window **slot, unsigned int frac_type);
void			new_window_get_fractals(t_window *win);
void			new_window_set_hooks(t_window *win);

void			draw(t_window *win);
void			draw_pixel(t_window *win, int x, int y, unsigned int color);

void			print_usage(void);
void			init_parameter(t_window *win);
void			next_fractal(t_window *win);
unsigned int	get_color(int nbr);

void			rect_resize(t_rect *rect, float ratio);
void			rect_key_move(t_rect *rect, float delta_x, float delta_y);
void			rect_set_mandelbrot(t_rect *rect);
void			rect_set_julia(t_rect *rect);
void			rect_set_burning_ship(t_rect *rect);

int				hook_expose(void *param);
int				hook_key_press(int keycode, void *param);
int				hook_mouse(int button, int x, int y, void *param);
int				hook_motion(int x, int y, void *param);

int				mandelbrot_set(t_window *win);
int				julia_set(t_window *win);
int				burning_ship_set(t_window *win);

#endif
