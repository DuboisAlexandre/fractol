/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 17:06:17 by adubois           #+#    #+#             */
/*   Updated: 2016/01/14 15:50:44 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns 1 if the character 'c' is part of the space characters list,
** 0 otherwise.
*/

int		ft_isspace(int c)
{
	unsigned char	chr;

	chr = (unsigned char)c;
	if (chr == '\t' || chr == '\n' || chr == '\v' ||
		chr == '\f' || chr == '\r' || chr == ' ')
		return (1);
	else
		return (0);
}
