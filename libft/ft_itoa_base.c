/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 13:15:57 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:37:14 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer on a new string containing the ASCI representation of
** the 'n' in base 'base' (min. 2 - max. 16).
*/

char	*ft_itoa_base(int n, int base)
{
	int		i;
	int		negative;
	char	*number;
	char	*charset;

	charset = "0123456789ABCDEF";
	if (base < 2 || base > 16)
		return (NULL);
	negative = (n < 0) ? 1 : 0;
	if ((number = ft_strnew(sizeof(char) * 32)) == NULL)
		return (NULL);
	i = 0;
	if (n == 0)
		number[i] = '0';
	while (n != 0)
	{
		number[i] = charset[ft_abs(n % base)];
		n /= base;
		i++;
	}
	if (negative)
		number[i] = '-';
	return (ft_strrev(number));
}
