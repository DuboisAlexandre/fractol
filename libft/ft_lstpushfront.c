/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpushfront.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 14:48:26 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:09:28 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Add the node 'new' at the beginning of the linked list pointed by 'alst'.
*/

void	ft_lstpushfront(t_list **alst, t_list *new)
{
	ft_lstadd(alst, new);
}
