/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 19:38:20 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:39:22 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

/*
** Returns a pointer on an allocated memory space of 'size' bytes.
*/

void	*ft_memalloc(size_t size)
{
	void	*ptr;

	if (size < 1)
		return (NULL);
	if ((ptr = malloc(size)) == NULL)
		return (NULL);
	ft_bzero(ptr, size);
	return (ptr);
}
