/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 15:48:41 by adubois           #+#    #+#             */
/*   Updated: 2016/01/14 18:49:38 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Set the value 'c' to the first 'len' bytes in the memory space pointed by
** 'b'.
*/

void	*ft_memset(void *b, int c, size_t len)
{
	long int			ptr;
	unsigned long int	cccc;

	ptr = (long int)b;
	cccc = (unsigned char)c;
	cccc |= cccc << 8;
	cccc |= cccc << 16;
	cccc |= cccc << 32;
	while (ptr % 8 != 0)
	{
		*((unsigned char *)ptr++) = (unsigned char)c;
		--len;
	}
	while (len >= 8)
	{
		*((unsigned long int *)ptr) = cccc;
		ptr += 8;
		len -= 8;
	}
	while (len--)
		*((unsigned char *)ptr++) = (unsigned char)c;
	return (b);
}
