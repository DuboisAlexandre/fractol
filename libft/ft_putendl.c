/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 20:31:50 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:30:05 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Prints the character string 's' on the standard output, followed by a new
** line character.
*/

void	ft_putendl(char const *s)
{
	ft_putendl_fd(s, 1);
}
