/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queuecount.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 13:42:42 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:52:52 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns the size of the queue 'queue'.
*/

int		ft_queuecount(t_queue *queue)
{
	if (queue == NULL)
		return (-1);
	return (ft_lstcount(queue->top));
}
