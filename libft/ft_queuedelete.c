/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queuedelete.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 13:47:22 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:33:07 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Delete the queue pointed by 'queue_ptr' and set the original pointer
** to NULL.
*/

void	ft_queuedelete(t_queue **queue_ptr)
{
	t_list	*top;

	if (queue_ptr != NULL && *queue_ptr != NULL)
	{
		top = (*queue_ptr)->top;
		ft_lstdel(&top, ft_lstnodefree);
		ft_memdel((void**)queue_ptr);
	}
}
