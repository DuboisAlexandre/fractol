/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 19:59:34 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:49:20 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Frees the character string pointed by 'as' and set the original pointer
** to NULL.
*/

void	ft_strdel(char **as)
{
	if (as != NULL)
		if (*as != NULL)
			ft_memdel((void**)as);
}
