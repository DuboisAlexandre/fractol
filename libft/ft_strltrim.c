/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strltrim.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 21:58:17 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:03:40 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a copy of the character string 'str', without the leading
** whitespaces. The whitespaces characters are ' ', '\n' and '\t'.
*/

char	*ft_strltrim(char const *str)
{
	size_t	i;
	char	*ptr;
	char	*result;

	if (str == NULL)
		return (NULL);
	ptr = (char*)str;
	i = 0;
	while (str[i])
	{
		if (str[i] != ' ' && str[i] != '\n' && str[i] != '\t')
		{
			ptr = (char*)str + i;
			break ;
		}
		i++;
	}
	if ((result = ft_strnew(ft_strlen(ptr))) == NULL)
		return (NULL);
	ft_strcpy(result, ptr);
	return (result);
}
