/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:55:02 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:19:35 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer to the first occurence of the character string 's2' in
** 's1'. No more than 'n' characters of 's1' are compared.
*/

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	j;

	if (ft_strlen(s2) == 0)
		return ((char*)s1);
	i = 0;
	while (s1[i] && i < n)
	{
		if (s1[i] == s2[0])
		{
			j = 0;
			while (s1[i + j] && i + j < n && s1[i + j] == s2[j])
			{
				if (!s2[j + 1])
					return ((char*)s1 + i);
				j++;
			}
		}
		i++;
	}
	return (NULL);
}
