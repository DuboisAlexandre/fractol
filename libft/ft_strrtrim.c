/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrtrim.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 21:51:41 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:23:41 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Delete the trailing whitespaces of the character string 'str' by moving
** backward the NULL terminator. The whitespace characters are ' ', '\n'
** and '\t'.
*/

char	*ft_strrtrim(char *str)
{
	size_t	i;
	char	*ptr;

	if (str == NULL)
		return (NULL);
	ptr = str - 1;
	i = 0;
	while (str[i])
	{
		if (str[i] != ' ' && str[i] != '\n' && str[i] != '\t')
			ptr = str + i;
		i++;
	}
	*(ptr + 1) = '\0';
	return (str);
}
