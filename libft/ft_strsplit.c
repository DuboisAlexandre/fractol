/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 23:50:16 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:25:35 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Splits the character string 's' using the character 'c'. Each substring is
** inserted in an array of string which is returned. There's no empty line in
** the array of strings.
*/

static char	*ft_strsplit_get_line(char const *s, char c)
{
	static size_t	i = 0;
	size_t			j;
	size_t			line_size;
	char			*line;
	char			*next_c;

	while (s[i] == c)
		i++;
	if ((next_c = ft_strchr(s + i, c)) != NULL)
		line_size = next_c - (s + i) + 1;
	else
		line_size = ft_strlen(s + i);
	if ((line = ft_strnew(sizeof(char*) * line_size)) == NULL)
		return (NULL);
	j = 0;
	while (s[i] && s[i] != c)
		line[j++] = s[i++];
	if (!line[0])
		i = 0;
	return (line);
}

static char	**ft_strsplit_free_tab(char ***tab, int size)
{
	while (size >= 0)
		ft_strdel(&(*tab[size--]));
	*tab = NULL;
	return (*tab);
}

char		**ft_strsplit(char const *s, char c)
{
	int		tab_size;
	int		i;
	char	**tab;

	if (s == NULL)
		return (NULL);
	tab_size = ft_strchr_count((char*)s, c) + 2;
	if ((tab = (char**)ft_memalloc(sizeof(char*) * tab_size)) == NULL)
		return (NULL);
	i = 0;
	while (1)
	{
		tab[i] = ft_strsplit_get_line(s, c);
		if (tab[i] == NULL)
			return (ft_strsplit_free_tab(&tab, i));
		if (!tab[i][0])
		{
			ft_strdel(&tab[i]);
			break ;
		}
		i++;
	}
	return (tab);
}
