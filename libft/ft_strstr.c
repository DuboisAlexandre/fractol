/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:43:31 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:27:34 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer to the next occurence of the character string 's2' in 's1'.
** If there's no occurence, NULL is returned.
*/

char	*ft_strstr(const char *s1, const char *s2)
{
	int		i;
	int		j;

	if (ft_strlen(s2) == 0)
		return ((char*)s1);
	i = 0;
	while (s1[i])
	{
		if (s1[i] == s2[0])
		{
			j = 0;
			while (s1[i + j] && s1[i + j] == s2[j])
			{
				if (!s2[j + 1])
					return ((char*)s1 + i);
				j++;
			}
		}
		i++;
	}
	return (NULL);
}
