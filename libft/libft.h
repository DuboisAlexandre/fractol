/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 16:31:23 by adubois           #+#    #+#             */
/*   Updated: 2016/01/14 18:28:02 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stdlib.h>
# include <unistd.h>
# include <string.h>

# define IS_BIG_ENDIAN (*(unsigned short int *)"\0\xFF" < 0x100)
# define GNL_BUFF_SIZE 1024

/*
**	Linked list template
*/

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

/*
** Stack template
*/

typedef struct		s_stack
{
	t_list			*top;
	int				size;
}					t_stack;

/*
** Queue template
*/

typedef struct		s_queue
{
	t_list			*top;
	t_list			*bottom;
	int				size;
}					t_queue;

/*
** GNL buffer
*/

typedef struct		s_gnl_file
{
	int					fd;
	int					start;
	int					status;
	char				buffer[GNL_BUFF_SIZE + 1];
	struct s_gnl_file	*next;
}					t_gnl_file;

/*
** Memory management
*/

void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src,
								int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);

/*
** Output
*/

void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);
void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putendl(char const *s);
void				ft_putnbr(int n);
void				ft_print_memory(const void *addr, unsigned int size);

/*
** File management
*/

int					ft_get_next_line(int const fd, char **line);

/*
** Char control
*/

int					ft_isupper(int c);
int					ft_islower(int c);
int					ft_isalpha(int c);
int					ft_isspace(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);

/*
** String control
*/

size_t				ft_strlen(const char *s);
size_t				ft_strnlen(const char *s, size_t maxlen);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
char				*ft_strcasechr(const char *s, int c);
char				*ft_strcasestr(const char *s1, const char *s2);
int					ft_strchr_count(char *str, int c);
int					ft_strstr_count(char *str, const char *needle);
int					ft_strcasechr_count(char *str, int c);
int					ft_strcasestr_count(char *str, const char *needle);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);

/*
** String management
*/

char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
char				*ft_strtoupper(char *str);
char				*ft_strtolower(char *str);
char				*ft_strdup(const char *s1);
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(char *dst, const char *src, size_t n);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strncat(char *s1, const char *s2, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strrev(char *str);
char				*ft_strrtrim(char *str);
char				*ft_strltrim(char const *str);
char				*ft_strtrim(char const *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				**ft_strsplit(char const *s, char c);
char				*ft_strnreplace(char *haystack, char *needle,
									char *replace, int n);
char				*ft_strreplace(char *haystack, char *needle, char *replace);
char				**ft_strarraysort(char **tab,
										int (*cmp)(const char *, const char *));
t_list				*ft_strsplittolst(char const *s, char c);

/*
**	Conversion
*/

int					ft_atoi(const char *str);
char				*ft_itoa(int n);
char				*ft_itoa_base(int n, int base);

/*
**	Mathematics
*/

int					ft_abs(int n);
int					ft_min(int a, int b);
int					ft_max(int a, int b);

/*
**	Linked list control
*/

int					ft_lstcount(t_list *lst);
void				ft_lstprintstr(t_list *list);

/*
**	Linked list management
*/

t_list				*ft_lstnew(void const *content, size_t content_size);
int					ft_lstnodestrcmp(const t_list *node1, const t_list *node2);
void				ft_lstnodefree(void *content, size_t size);
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstpushfront(t_list **alst, t_list *new);
void				ft_lstpushback(t_list **alst, t_list *new);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
void				ft_lstsort(t_list **list, int (*cmp)(const t_list *,
															const t_list *));

/*
** Stack control
*/

int					ft_stackcount(t_stack *stack);

/*
** Stack management
*/

t_stack				*ft_stackcreate(void);
void				ft_stackdelete(t_stack **stack_ptr);
int					ft_stackpush(t_stack *stack, t_list *node);
t_list				*ft_stackpop(t_stack *stack);

/*
** Queue control
*/

int					ft_queuecount(t_queue *queue);

/*
** Queue management
*/

t_queue				*ft_queuecreate(void);
void				ft_queuedelete(t_queue **queue_ptr);
int					ft_queuepush(t_queue *queue, t_list *node);
t_list				*ft_queuepop(t_queue *queue);

#endif
