/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 13:29:06 by adubois           #+#    #+#             */
/*   Updated: 2016/01/22 13:43:25 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			draw(t_window *win)
{
	float	x;
	float	y;
	int		nbr_iter;

	nbr_iter = 0;
	ft_memset(win->data, 0, WIN_HEIGHT * win->lsize);
	y = WIN_HEIGHT;
	while (y--)
	{
		win->var_y = win->rect.y_max - y
						* (win->rect.y_max - win->rect.y_min)
						/ (WIN_HEIGHT - 1);
		x = WIN_WIDTH;
		while (x--)
		{
			win->var_x = win->rect.x_min + x
							* (win->rect.x_max - win->rect.x_min)
							/ (WIN_WIDTH - 1);
			nbr_iter = win->fractals[win->frac_type](win);
			draw_pixel(win, x, y, get_color(nbr_iter));
		}
	}
	mlx_put_image_to_window(win->mlx, win->ptr, win->img, 0, 0);
}

void			draw_pixel(t_window *win, int x, int y, unsigned int color)
{
	unsigned int	offset;

	offset = y * win->lsize;
	offset += x * (win->bpp / 8);
	*((unsigned int *)(win->data + offset)) = color;
}
