/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractals.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 13:29:40 by adubois           #+#    #+#             */
/*   Updated: 2016/01/21 19:22:47 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		mandelbrot_set(t_window *win)
{
	float	x;
	float	y;
	float	swap;
	int		dwell;

	dwell = win->dwell;
	x = 0;
	y = 0;
	while (--dwell)
	{
		swap = x * x - y * y + win->var_x;
		y = 2 * x * y + win->var_y;
		x = swap;
		if (x * x + y * y > 4)
			break ;
	}
	return (dwell);
}

int		julia_set(t_window *win)
{
	float	x;
	float	y;
	float	swap;
	int		dwell;

	dwell = win->dwell;
	x = win->var_x;
	y = win->var_y;
	while (--dwell)
	{
		swap = x * x - y * y + win->real;
		y = 2 * x * y + win->imag;
		x = swap;
		if (x * x + y * y > 4)
			break ;
	}
	return (dwell);
}

int		burning_ship_set(t_window *win)
{
	float	x;
	float	y;
	float	swap;
	int		dwell;

	dwell = win->dwell;
	x = 0;
	y = 0;
	while (--dwell)
	{
		swap = x * x - y * y + win->var_x;
		y = x * y;
		y = (y >= 0) ? y : -1 * y;
		y = y + y - win->var_y;
		x = swap;
		if (x * x + y * y > 4)
			break ;
	}
	return (dwell);
}
