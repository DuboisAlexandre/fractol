/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 05:47:35 by adubois           #+#    #+#             */
/*   Updated: 2016/01/22 15:22:44 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		main(int argc, char **argv)
{
	t_fractol	frac;
	t_window	**win;

	if (argc < 2)
		print_usage();
	else
	{
		frac.mlx = mlx_init();
		frac.win = NULL;
		win = &frac.win;
		while (--argc > 0)
		{
			if (ft_strcmp(argv[argc], "mandelbrot") == 0)
				new_window(frac.mlx, win, 0);
			else if (ft_strcmp(argv[argc], "julia") == 0)
				new_window(frac.mlx, win, 1);
			else if (ft_strcmp(argv[argc], "burning_ship") == 0)
				new_window(frac.mlx, win, 2);
			else
				print_usage();
			win = &(*win)->next;
		}
		mlx_loop(frac.mlx);
	}
	return (0);
}

void	*new_window(void *mlx, t_window **slot, unsigned int frac_type)
{
	t_window	*win;

	if (!(win = (t_window *)malloc(sizeof(t_window)))
		|| !(win->ptr = mlx_new_window(mlx, WIN_WIDTH, WIN_HEIGHT,
										WIN_TITLE))
		|| !(win->img = mlx_new_image(mlx, WIN_WIDTH, WIN_HEIGHT)))
		return (NULL);
	win->frac_type = frac_type;
	win->dwell = DEFAULT_DWELL;
	win->mouse_x = 0;
	win->mouse_y = 0;
	win->mlx = mlx;
	win->data = mlx_get_data_addr(win->img, &win->bpp, &win->lsize,
									&win->bendian);
	new_window_get_fractals(win);
	new_window_set_hooks(win);
	init_parameter(win);
	*slot = win;
	return (win);
}

void	new_window_set_hooks(t_window *win)
{
	mlx_hook(win->ptr, EXPOSE, EXPOSURE_MASK,
				hook_expose, (void *)win);
	mlx_hook(win->ptr, KEY_PRESS, KEY_PRESS_MASK,
				hook_key_press, (void *)win);
	mlx_hook(win->ptr, BUTTON_PRESS, BUTTON_PRESS_MASK,
				hook_mouse, (void *)win);
	mlx_hook(win->ptr, MOTION_NOTIFY, POINTER_MOTION_MASK,
				hook_motion, (void *)win);
}

void	new_window_get_fractals(t_window *win)
{
	win->fractals[0] = mandelbrot_set;
	win->fractals[1] = julia_set;
	win->fractals[2] = burning_ship_set;
}
