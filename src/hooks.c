/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 13:26:08 by adubois           #+#    #+#             */
/*   Updated: 2016/01/22 15:23:33 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		hook_expose(void *param)
{
	draw((t_window *)param);
	return (0);
}

int		hook_mouse(int button, int x, int y, void *param)
{
	t_rect		*rect;
	float		delta_x;
	float		delta_y;
	float		real;
	float		imag;

	if (y < 0)
		return (0);
	rect = &((t_window *)param)->rect;
	imag = rect->y_max - y * (rect->y_max - rect->y_min) / (WIN_HEIGHT - 1);
	real = rect->x_min + x * (rect->x_max - rect->x_min) / (WIN_WIDTH - 1);
	if (button == 5)
		rect_resize(rect, 1.1);
	if (button == 4)
		rect_resize(rect, 0.9);
	delta_y = imag - (rect->y_max - y * (rect->y_max - rect->y_min)
										/ (WIN_HEIGHT - 1));
	delta_x = real - (rect->x_min + x * (rect->x_max - rect->x_min)
										/ (WIN_WIDTH - 1));
	rect->x_max += delta_x;
	rect->x_min += delta_x;
	rect->y_max += delta_y;
	rect->y_min += delta_y;
	draw((t_window *)param);
	return (0);
}

int		hook_key_press(int keycode, void *param)
{
	t_window	*win;

	win = (t_window *)param;
	if (keycode == 53)
		exit(0);
	else if (keycode == 78)
		hook_mouse(5, win->mouse_x, win->mouse_y, param);
	else if (keycode == 69)
		hook_mouse(4, win->mouse_x, win->mouse_y, param);
	else if (keycode == 123)
		rect_key_move(&win->rect, -0.01, 0);
	else if (keycode == 124)
		rect_key_move(&win->rect, 0.01, 0);
	else if (keycode == 125)
		rect_key_move(&win->rect, 0, -0.01);
	else if (keycode == 126)
		rect_key_move(&win->rect, 0, 0.01);
	else if (keycode == 49)
		next_fractal(win);
	else if (keycode == 36)
		init_parameter(win);
	draw(win);
	return (0);
}

int		hook_motion(int x, int y, void *param)
{
	t_window	*win;

	win = (t_window *)param;
	if (win->mouse_x == 0)
		win->mouse_x = x;
	if (win->mouse_y == 0)
		win->mouse_y = y;
	win->real += (win->real + (win->mouse_x - x)) / 20000;
	win->imag += (win->real + (win->mouse_y - y)) / 20000;
	win->mouse_x = x;
	win->mouse_y = y;
	draw(win);
	return (0);
}
