/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rect.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 11:28:33 by adubois           #+#    #+#             */
/*   Updated: 2016/01/22 13:56:10 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	rect_resize(t_rect *rect, float ratio)
{
	rect->x_max *= ratio;
	rect->x_min *= ratio;
	rect->y_max *= ratio;
	rect->y_min *= ratio;
}

void	rect_key_move(t_rect *rect, float delta_x, float delta_y)
{
	rect->x_min += delta_x;
	rect->x_max += delta_x;
	rect->y_min += delta_y;
	rect->y_max += delta_y;
}

void	rect_set_mandelbrot(t_rect *rect)
{
	rect->x_min = -2.0;
	rect->x_max = 1.0;
	rect->y_min = -1.5;
	rect->y_max = 1.5;
}

void	rect_set_julia(t_rect *rect)
{
	rect->x_min = -1.6;
	rect->x_max = 1.6;
	rect->y_min = -1.6;
	rect->y_max = 1.6;
}

void	rect_set_burning_ship(t_rect *rect)
{
	rect->x_min = -2.3;
	rect->x_max = 1.7;
	rect->y_min = -1.6;
	rect->y_max = 2.4;
}
