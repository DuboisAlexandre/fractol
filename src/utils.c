/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 11:03:19 by adubois           #+#    #+#             */
/*   Updated: 2016/01/22 15:11:00 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			print_usage(void)
{
	ft_putstr("Usage: ./fractol [fractal1] [fractal2] ...\n\n");
	ft_putstr("Available fractals:\n\n");
	ft_putstr("mandelbrot\njulia\nburning_ship\n\n");
	exit(1);
}

void			init_parameter(t_window *win)
{
	win->real = 0.353;
	win->imag = 0.288;
	if (win->frac_type == 0)
		rect_set_mandelbrot(&win->rect);
	if (win->frac_type == 1)
		rect_set_julia(&win->rect);
	if (win->frac_type == 2)
		rect_set_burning_ship(&win->rect);
}

void			next_fractal(t_window *win)
{
	win->frac_type++;
	win->frac_type = (win->frac_type > 2) ? 0 : win->frac_type;
	init_parameter(win);
}

unsigned int	get_color(int nbr)
{
	if (nbr <= 0)
		return (0x0);
	else if (nbr <= 69)
		return (0xff0000);
	else if (nbr <= 85)
		return (0xff00ff);
	else if (nbr <= 90)
		return (0x7f00ff);
	else if (nbr <= 95)
		return (0x0000ff);
	else if (nbr <= 96)
		return (0x000099);
	else if (nbr <= 97)
		return (0x000066);
	else if (nbr <= 98)
		return (0x000033);
	else
		return (0x0);
}
